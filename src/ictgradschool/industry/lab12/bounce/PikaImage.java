package ictgradschool.industry.lab12.bounce;

import javax.imageio.ImageIO;
import javax.xml.transform.Source;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by J Won on 1/05/2017.
 */
public class PikaImage extends Shape {

    private BufferedImage pika;
    private int width;
    private int height;
    private String pic;
    private Image sizedImage;

    public void inputImage() {
        try {
            pika = ImageIO.read(new File(pic));
            super.fWidth = (int)(pika.getWidth(null)*0.1);
            super.fHeight = (int)(pika.getHeight(null)*0.1);
            sizedImage = pika.getScaledInstance(fWidth,fHeight, Image.SCALE_DEFAULT);
        } catch (IOException exception) {
            System.out.println("Wrong filename or path");
        }
    }

    public PikaImage() {
        super();
    }

    public PikaImage (int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public PikaImage(int x, int y, int deltaX, int deltaY, int width, int height,String source) {
        super(x, y, deltaX, deltaY, width, height);
        pic = source;
        inputImage();
    }

    public PikaImage(int x, int y, int deltaX, int deltaY, String source) {
        super(x, y, deltaX, deltaY);
        pic = source;
        inputImage();
    }

    @Override
    public void paint(Painter painter) {

        painter.drawImage(sizedImage,fX,fY,null);
    }
}
