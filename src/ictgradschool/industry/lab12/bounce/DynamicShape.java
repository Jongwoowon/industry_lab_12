package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Created by J Won on 1/05/2017.
 */
public class DynamicShape extends Shape {

    private Color color = Color.black;
    private int selection = 1;
    private int thisMirrorY;
    private int thisMirrorX;

    /**
     * Default constructor that creates a ovalShape
     */
    public DynamicShape() {
        super();
    }

    public DynamicShape(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
        thisMirrorX = deltaX;
        thisMirrorY = deltaY;
    }

    public DynamicShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
        thisMirrorX = deltaX;
        thisMirrorY = deltaY;
    }

    public DynamicShape(int x, int y, int deltaX, int deltaY, Color color) {
        super(x,y,deltaX,deltaY);
        thisMirrorX = deltaX;
        thisMirrorY = deltaY;
        this.color = color;
    }

    @Override
    public void paint(Painter painter) {
        if (thisMirrorX == fDeltaX && thisMirrorY == fDeltaY) {
           paintingThis(painter, selection);
        }
        else if (thisMirrorX != fDeltaX && thisMirrorY == fDeltaY) {
            paintingThis(painter, 1);
            selection = 1;
        }
        else {
            paintingThis(painter, 2);
            selection = 2;
        }
        thisMirrorX = fDeltaX;
        thisMirrorY = fDeltaY;
    }

    public void paintingThis(Painter painter, int selection) {
        if (selection == 1) {
            painter.setColor(Color.black);
            painter.drawRect(fX, fY, fWidth, fHeight);
        }
        else {
            painter.setColor(color);
            painter.fillRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.black);
            painter.drawRect(fX, fY, fWidth, fHeight);
        }
    }


}

