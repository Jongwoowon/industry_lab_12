package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Created by jwon117 on 11/04/2017.
 */
public class WeirdShape extends Shape {
    /**
     * Default constructor that creates a ovalShape
     */
    public WeirdShape() {
        super();
    }


    public WeirdShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public WeirdShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void paint(Painter painter) {

        int[] GemX = {fX, fX + (fWidth - 20), fX + 20, fX + fWidth, fX + 20, fX + (fWidth - 20)};
        int[] GemY = {fY + (fHeight / 2), fY, fY, fY + (fHeight / 2), fY + fHeight, fY + fHeight};
        Polygon newPolygon = new Polygon(GemX, GemY, 6);

        painter.drawPolygon(newPolygon);

    }

}