package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Created by jwon117 on 11/04/2017.
 */
public class GemShape extends Shape {
    /**
     * Default constructor that creates a ovalShape
     */
    public GemShape() {
        super();
    }


    public GemShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void paint(Painter painter) {

        if (fWidth <= 40) {
            int[] GemX = {(fX + fWidth / 2), fX + fWidth, fX + fWidth / 2, 0 + fX};
            int[] GemY = {fY, fY + fHeight / 2, fY + fHeight, fY + fHeight / 2};

            Polygon newPolygon = new Polygon(GemX, GemY, 4);

            painter.drawPolygon(newPolygon);
        }
        else {

        int [] GemX = {fX+20, fWidth+fX-20, fWidth+fX, fWidth+fX-20, fX+20, fX};
        int [] GemY = {fY, fY, fY+fHeight/2, fY+fHeight, fHeight+fY, fY + fHeight/2};
            Polygon newPolygon = new Polygon(GemX, GemY, 6);

            painter.drawPolygon(newPolygon);

        }


    }
}
