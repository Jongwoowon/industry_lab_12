package ictgradschool.industry.lab12.bounce;

/**
 * Created by jwon117 on 11/04/2017.
 * Class to represent a simple rectangle.
 */
public class OvalShape extends Shape {
    /**
     * Default constructor that creates a ovalShape
     */
public OvalShape() { super();}


public OvalShape(int x, int y, int deltaX, int deltaY){super(x,y,deltaX,deltaY);}

    public OvalShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }

    @Override
    public void paint(Painter painter) {
        painter.drawOval(fX, fY, fWidth, fHeight);
    }


}
